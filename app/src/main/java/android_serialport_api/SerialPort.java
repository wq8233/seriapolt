

package android_serialport_api;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.util.Log;

/**
 * 该类是获取串口的类，从而可以获取到串口的输入流和输出流，如果想简单实现可以直接将该类和包一起复制到自己的项目下
 */
public class SerialPort {

	private static final String TAG = "SerialPort";

	/**
	 * Do not remove or rename the field mFd: it is used by native method close();
	 * 不要改变这个类的包名，因为他用了native调用.so库
	 */
	private FileDescriptor mFd;
	private FileInputStream mFileInputStream;
	private FileOutputStream mFileOutputStream;

	public SerialPort(File device, int baudrate, int flags) throws SecurityException, IOException {

		/* Check access permission */
		if (!device.canRead() || !device.canWrite()) {
			try {
				/* Missing read/write permission, trying to chmod the file */
				Process su;
				su = Runtime.getRuntime().exec("/system/bin/su");
				String cmd = "chmod 666 " + device.getAbsolutePath() + "\n"
						+ "exit\n";
				su.getOutputStream().write(cmd.getBytes());
				if ((su.waitFor() != 0) || !device.canRead()
						|| !device.canWrite()) {
					throw new SecurityException();
				}
			} catch (Exception e) {
				e.printStackTrace();
				throw new SecurityException();
			}
		}

		mFd = open(device.getAbsolutePath(), baudrate, flags);
		if (mFd == null) {
			Log.e(TAG, "native open returns null");
			throw new IOException();
		}
		mFileInputStream = new FileInputStream(mFd);
		mFileOutputStream = new FileOutputStream(mFd);
	}

	/**
	 * 获取串口的输入流
	 * @return
     */
	public InputStream getInputStream() {
		return mFileInputStream;
	}

	/**
	 * 获取串口输出流
	 * @return
     */
	public OutputStream getOutputStream() {
		return mFileOutputStream;
	}

	//jni调用
	private native static FileDescriptor open(String path, int baudrate, int flags);
	public native void close();

	static {
		System.loadLibrary("serial_port");
	}
}
